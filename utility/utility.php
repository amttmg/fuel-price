<?php
function getTodayDate($data)
{
    $today = date(get_option('date_format'), strtotime(getLastDate()));
    if (!$data) {
        return $today;
    }

    return str_replace("[today]", $today, $data);
}

function getTodayPetrolPrice($atts)
{
    $today    = date("Y-m-d", strtotime(getLastDate()));
    $cityName = $atts['city'] ?? $_GET['city'];
    $price    = getFuelPriceByDate($cityName, $today);

    return $price[0]->petrol;
}

function getTodayDieselPrice($atts)
{
    $today    = date("Y-m-d", strtotime(getLastDate()));
    $cityName = $atts['city'] ?? $_GET['city'];
    $price    = getFuelPriceByDate($cityName, $today);

    return $price[0]->diesel;
}

function getOnlyTodayGoldPrice($atts)
{
    $today    = date("Y-m-d", strtotime(getLastDate()));
    $cityName = $atts['city'] ?? $_GET['city'];
    $gram     = $atts['gram'] ?? 1;
    $price    = getGoldPriceByDate($cityName, $today);
    if ($atts['k'] == '22') {
        return $price[0]->price1*$gram;
    } elseif ($atts['k'] == '24') {
        return $price[0]->price2*$gram;
    } else {
        return '[please send parameter k=22 or k=24 on shortcode]';
    }
}
