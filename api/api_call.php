<?php
function getData($url)
{
    //loge("api call url", $url);
    $head = '{}';
    try {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'x-rapidapi-host: '.get_option('api_host'),
            'x-rapidapi-key: '.get_option('api_key'),
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($ch, CURLOPT_TIMEOUT, 5); //timeout in seconds
        $head     = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if (empty($head)) {
            $head = '{}';
        }
        //loge("api call success", "Records fetched");
    } catch (Exception $ex) {
        //loge("api call error", $ex->getMessage(), "error");
    }

    return json_decode($head);;
}

function getContent($url)
{
    $agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERAGENT, $agent);
    curl_setopt($ch, CURLOPT_URL, $url);

    return curl_exec($ch);
}

function getDataFromSource()
{
    $dataPetrol = getDataFromSite("https://www.bankbazaar.com/fuel/petrol-price-india.html", 1);
    $dataDiesel = getDataFromSite("https://www.bankbazaar.com/fuel/diesel-price-india.html", 0);

    return array_merge_recursive($dataPetrol, $dataDiesel);
}

function getDataFromSite($url, $tableIndex)
{
    $data = getContent($url);
    $dom  = new domDocument;

    @$dom->loadHTML($data);
    $dom->preserveWhiteSpace = false;
    $tables                  = $dom->getElementsByTagName('table');

    $rows   = $tables->item($tableIndex)->getElementsByTagName('tr');

    $master = [];
    foreach ($rows as $row) {
        $cols         = $row->getElementsByTagName('td');
        $key          = str_replace(" ", "", $cols[0]->nodeValue);
        $value        = str_replace("₹ ", "", $cols[1]->nodeValue);
        $master[ucfirst(strtolower($key))] = $value;
    }

    return array_slice($master, 1);
}

