<?php
/**
 * Plugin Name: Fuel Price In India
 * Plugin URI: amrittamang.com.np
 * Description: Display the fuel price in india shortcode: [fuel-price]
 * Version: 2.3
 * Text Domain: tbare-wordpress-plugin-demo
 * Author: Amrit Tamang
 * Author URI: amrittamang.com.np
 */
include('database/create_database.php');
include('database/dbFunctions.php');
include('database/dbGold.php');
include('api/api_call.php');
include('logs/logs.php');
include('options/setting.php');
include('utility/utility.php');
add_filter('the_title', 'getTodayDate');
add_filter('wp_title', 'getTodayDate');
add_shortcode('today', 'getTodayDate');
add_shortcode('today-petrol-price', 'getTodayPetrolPrice');
add_shortcode('today-diesel-price', 'getTodayDieselPrice');
add_shortcode('today-gold-price', 'getOnlyTodayGoldPrice');

register_activation_hook(__FILE__, 'create_database');
add_shortcode('fuel-price', 'main');
add_shortcode('fuel-price-all', 'showAll');
add_shortcode('fuel-price-trend', 'showTrend');
add_shortcode('gold-price-trend', 'showGoldTrend');
add_shortcode('gold-price', 'showGoldPrice');
add_shortcode('gold-price-all', 'showGoldPrice');
add_action('rest_api_init', function () {
    register_rest_route('fetch', 'data', [
        'methods'  => 'GET',
        'callback' => 'fetch_data',
    ]);
});
function fetch_data()
{
    //insertStatesAndCities();
    fetchGoldPrices();
    fetchDataFromApiAndInsert();
}

function main($atts)
{
    $stateId  = $atts['stateid'];
    $view     = $atts['view'];
    $cityId   = $atts['cityid'] ?? $_GET['cityId'];
    $cityName = $atts['city'] ?? $_GET['city'];
    if ($stateId && !$cityId && !$cityName) {
        $data = getStateDataDB($stateId);

        ob_start();
        if ($view == "graph") {
            include('view/state_graph_view.php');
        } else {
            include('view/state_table_view.php');
        }

        return ob_get_clean();
    } elseif ($cityId || $cityName) {
        if ($cityId) {
            $data = getCityDataDB($cityId);
        } else {
            $data = getCityDataDBByName($cityName);
        }
        ob_start();
        if ($view == "graph") {
            include('view/city_graph_view.php');
        } else {
            include('view/city_table_view.php');
        }

        return ob_get_clean();
    }
}

function showAll($atts)
{
    $cityId   = $atts['cityid'] ?? $_GET['cityId'];
    $view     = $atts['view'];
    $cityName = $atts['city'] ?? $_GET['city'];
    if ($cityId || $cityName) {
        $master = ["cityId" => $cityId, "city" => $cityName];
        echo main($master);
    } else {
        $data = getAllCityData();

        ob_start();
        if ($view == "graph") {
            include('view/state_graph_view.php');
        } else {
            include('view/state_table_view.php');
        }

        return ob_get_clean();
    }
}

function showGoldPrice($attr)
{
    $cityName = $attr['city'] ?? $_GET['city'];
    $gram     = $attr['gram'] ?? 1;
    $view     = $attr['view'];
    if ($cityName) {
        $data = getHistoryGoldPrice($cityName, $gram);
        ob_start();
        if ($view == "graph") {
            include('view/gold_city_history_graph_view.php');
        } else {
            include('view/gold_city_history_table_view.php');
        }


        return ob_get_clean();
    } else {
        $data = getTodayGoldPrice($gram);
        ob_start();
        if ($view == "graph") {
            include('view/gold_city_graph_view.php');
        } else {
            include('view/gold_city_table_view.php');
        }

        return ob_get_clean();
    }

}

function showTrend($attr)
{
    $gram     = $attr['gram'] ?? 1;
    $cityName = $attr['city'] ?? $_GET['city'];
    $fuelType = $attr['fuel_type'] ?? $_GET['fuel_type'];

    $data = getCityTrend($cityName, $fuelType);

    ob_start();
    include('view/fuel_price_trend.php');

    return ob_get_clean();

}

function showGoldTrend($attr)
{
    $cityName = $attr['city'] ?? $_GET['city'];
    $k        = $attr['k'] ?? $_GET['k'];
    $gram     = $attr['gram'] ?? 1;
    if ($k == 22) {
        $fuelType = 'price1';
        $label    = "22K Gold";
    } else {
        $fuelType = 'price2';
        $label    = "24K Gold";
    }
    $data = getCityGoldTrend($cityName, $fuelType);
    ob_start();
    include('view/fuel_price_trend.php');

    return ob_get_clean();

}
