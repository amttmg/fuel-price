<?php
function fetchGoldPrices()
{
    $data = file_get_contents("https://www.fresherslive.com/gold-rate-today");
    $dom  = new domDocument;

    @$dom->loadHTML($data);
    $dom->preserveWhiteSpace = false;
    $tables                  = $dom->getElementsByTagName('table');

    $rows   = $tables->item(1)->getElementsByTagName('tr');
    $master = [];
    $today  = date("Y-m-d");
    foreach ($rows as $row) {
        $cols = $row->getElementsByTagName('td');
        $row  = [];
        foreach ($cols as $cell) {
            $row[] = "'".str_replace(",", "", str_replace("₹", "", $cell->nodeValue))."'";
        }
        if (count($row)) {
            $master[] = "('".$today."',".implode(",", $row).")";
        }
    }
    global $wpdb;
    $tableName = $wpdb->prefix."fuel_gold";
    $sqlDel    = "delete from $tableName where date_time='".date("Y-m-d")."'";
    $sql       = "insert into $tableName(date_time, city, price1, change1, price2, change2) values".implode(",",
            $master);

    $wpdb->query($sqlDel);
    $wpdb->query($sql);
}

function getTodayGoldPrice($gram)
{
    global $wpdb;
    $tableName = $wpdb->prefix."fuel_gold";
    $columns   = "id, date_time, city, price1*$gram as price1, price2*$gram as price2, change1, change2";

    $sql       = "select $columns from $tableName where date_time=(select max(date_time) from $tableName)";

    return $wpdb->get_results($sql);
}

function getHistoryGoldPrice($city, $gram, $top = 10)
{
    global $wpdb;
    $tableName = $wpdb->prefix.'fuel_gold';
    $columns   = "id, date_time, city, price1*$gram as price1, price2*$gram as price2, change1, change2";
    $sql       = "SELECT $columns FROM $tableName WHERE LOWER(REPLACE(`city`, ' ', ''))=%s order by date_time desc limit %d";
    $query     = $wpdb->prepare($sql, strtolower(trim($city)), $top);

    return $wpdb->get_results($query);
}

//Trend
function getCityGoldTrend($cityName, $fuelType, $months = 12)
{
    $today  = date_create(date("Y-m-01"));
    $master = [];
    for ($i = 0; $i < $months; $i++) {
        $year      = date("Y", strtotime("-".$i." months"));
        $monthName = date("F", strtotime("-".$i." months"));
        $month     = date("m", strtotime("-".$i." months"));
        $trend     = getGoldTrendOnMonth($cityName, $fuelType, $year, $month);
        if ($trend) {
            $master[$monthName." ".$year] = $trend;
        }
    }

    return $master;
}

function getGoldTrendOnMonth($cityName, $fuelType, $year, $month)
{
    $lastDate  = getLastDateOfMonthGold($cityName, $year, $month, "max");
    $firstDate = getLastDateOfMonthGold($cityName, $year, $month, "min");

    $firstDayRecord = getPriceOnDayGold($cityName, $firstDate);
    $lastDayRecord  = getPriceOnDayGold($cityName, $lastDate);

    if (!$firstDayRecord && !$lastDayRecord) {
        return [];
    }

    $firstPrice = (float)$firstDayRecord->$fuelType;
    $lastPrice  = (float)$lastDayRecord->$fuelType;

    $changePercentage = (float)(($lastPrice - $firstPrice) * 100 / $lastPrice);


    return [
        "first"   => $firstDayRecord,
        "last"    => $lastDayRecord,
        "highest" => getMaxOrMinFuelPriceGold($cityName, $fuelType, $year, $month, "max"),
        "lowest"  => getMaxOrMinFuelPriceGold($cityName, $fuelType, $year, $month, "min"),
        "trend"   => ($lastDayRecord > $firstDayRecord) ? "Increasing" : "Decreasing",
        "change"  => round($changePercentage, 2),
    ];
}

function getMaxOrMinFuelPriceGold($cityName, $fuelType, $year, $month, $clu)
{
    global $wpdb;
    $tableName = $wpdb->prefix.'fuel_gold';
    $sql       = "SELECT $fuelType as price, DATE_FORMAT(date_time, '%M') as month, GROUP_CONCAT(DATE_FORMAT(date_time, '%D')) as date from $tableName WHERE $fuelType=(select $clu($fuelType) from $tableName WHERE month(date_time)='%s' and year(date_time)='%s' and city='%s') and  month(date_time)='%s' and year(date_time)='%s' and city='%s' group by $fuelType";

    $query = $wpdb->prepare($sql, $month, $year, $cityName, $month, $year, $cityName);

    return $wpdb->get_results($query)[0];
}

function getLastDateOfMonthGold($cityName, $year, $month, $clu)
{
    global $wpdb;
    $tableName = $wpdb->prefix.'fuel_gold';
    $sql       = "select $clu(date_time) as date from $tableName where city=%s and month(date_time)=$month and year(date_time)=$year";

    $query = $wpdb->prepare($sql, $cityName);

    return date('Y-m-d', strtotime($wpdb->get_results($query)[0]->date));

}

function getPriceOnDayGold($cityName, $date)
{
    global $wpdb;
    $tableName   = $wpdb->prefix.'fuel_gold';
    $columnNames = "DATE_FORMAT(date_time, '%D, %M') as day, date_time, city, price1, price2, change1, change2";
    $sql         = "select $columnNames from $tableName WHERE city=%s and date(date_time)=%s";
    $query       = $wpdb->prepare($sql, $cityName, $date);
    $result      = $wpdb->get_results($query);

    if (count($result)) {
        return $result[0];
    }

    return [];
}
