<?php
function getCityDetails($cityId)
{
    global $wpdb;
    $city_table_name = $wpdb->prefix.'fuel_cities';

    $sql = "SELECT * FROM $city_table_name where id=%d";

    $query = $wpdb->prepare($sql, $cityId);

    $city = $wpdb->get_results($query);

    return $city ? $city[0] : [];
}

function getStateDetails($StateId)
{
    global $wpdb;
    $tableName = $wpdb->prefix.'fuel_States';

    $sql = "SELECT * FROM $tableName where id=%d";

    $query = $wpdb->prepare($sql, $StateId);

    $state = $wpdb->get_results($query);

    return $state ? $state[0] : [];
}

function getCities($stateId = 0)
{
    global $wpdb;
    $cities_table = $wpdb->prefix.'fuel_cities';

    $sql = "SELECT * FROM $cities_table ";
    if ($stateId) {
        $sql .= "where stateId=".$stateId;
    }

    return $wpdb->get_results($sql);
}

function getCitiesByName($name)
{
    global $wpdb;
    $cities_table = $wpdb->prefix.'fuel_cities';

    $sql = "SELECT * FROM $cities_table ";
    if ($name) {
        $sql .= "where city_name='".$name."'";
    }

    return $wpdb->get_results($sql);
}

function getStates()
{
    global $wpdb;
    $tableName = $wpdb->prefix.'fuel_states';

    $sql = "SELECT * FROM $tableName ";

    return $wpdb->get_results($sql);
}

function getStateWithCities()
{
    $states = getStates();
    foreach ($states as $state) {
        $state->cities = getCities($state->id);
    }

    return $states;

}

function getYesterdayData($cityName)
{
    global $wpdb;
    $tableName = $wpdb->prefix.'fuel_prices';

    $query = "select petrol, diesel from $tableName where city_name='$cityName' and date(date_time)='"
        .date('Y-m-d', strtotime("-1 days"))."'";
    $data  = $wpdb->get_results($query);

    return count($data) ? $data[0] : [];
}

function fetchDataFromApiAndInsert()
{
    $data    = getDataFromSource();
    $success = '';
    foreach ($data as $cityName => $price) {
        $cityData      = getCitiesByName($cityName);
        $yesterdayData = getYesterdayData($cityName);
        if ($cityData) {
            $cityData      = $cityData[0];
            $price['date'] = date('Y-m-d');
            if ($yesterdayData) {
                $price['diff0'] = ($price[0] - $yesterdayData->petrol);
                $price['diff1'] = ($price[1] - $yesterdayData->diesel);
            } else {
                $price['diff0'] = 0;
                $price['diff1'] = 0;
            }
            $success .= insertOrUpdateData($price, $cityData);
        }
    }
    loge("Report", $success);
}

function insertOrUpdateData($price, $cityData)
{
    if (isExist($cityData->cityId, $price['date'])) {
        updateCityData($price, $cityData);

        return 2;
    } else {
        insertCityData($price, $cityData);

        return 1;
    }
}

function updateCityData($price, $cityDetails)
{
    global $wpdb;
    $tableName = $wpdb->prefix.'fuel_prices';

    $sql   = "update $tableName set currency=%s, createdAt=%s, changeText=%s, dieselDiff=%f, petrolDiff=%f, petrol=%f, diesel=%f where cityId=$cityDetails->cityId and date(date_time)='".$price['date']."'";
    $query = $wpdb->prepare($sql,
        "INR",
        date("Y-m-d"),
        "",
        $price['diff0'],
        $price['diff1'],
        $price[0],
        $price[1]
    );

    $wpdb->query($query);
}

function insertCityData($price, $cityDetails)
{
    global $wpdb;
    $tableName = $wpdb->prefix.'fuel_prices';

    $sql   = "insert into $tableName(date_time, cityId, stateId, city_name, stateName, currency, createdAt, changeText, dieselDiff, petrolDiff, petrol, diesel) values(%s, %d, %d, %s, %s, %s, %s, %s, %f, %f, %f, %f)";
    $query = $wpdb->prepare($sql,
        $price['date'],
        $cityDetails->cityId,
        $cityDetails->stateId,
        $cityDetails->city_name,
        $cityDetails->stateName,
        "INR",
        date("Y-m-d"),
        "",
        $price['diff0'],
        $price['diff1'],
        $price[0],
        $price[1]);
    $wpdb->query($query);
}

function isExist($cityId, $date)
{
    global $wpdb;
    $tableName = $wpdb->prefix.'fuel_prices';

    $sql    = "SELECT * FROM $tableName where cityId=$cityId and date(date_time)='".$date."'";
    $result = $wpdb->get_results($sql);

    return count($result);
}

function getStateDataDB($stateId)
{
    global $wpdb;
    $tableName   = $wpdb->prefix.'fuel_prices';
    $columnNames = "city_name, currency, dieselDiff, petrolDiff, petrol, diesel";
    $sql         = "SELECT  $columnNames FROM $tableName WHERE date(date_time)=(SELECT max(date_time) FROM $tableName) and stateId=%d";
    $query       = $wpdb->prepare($sql, $stateId);
    $result      = $wpdb->get_results($query);

    return $result;
}

function getAllCityData()
{
    global $wpdb;
    $cityTableName = $wpdb->prefix.'fuel_cities';
    $tableName     = $wpdb->prefix.'fuel_prices';

    $optionsCities = get_option('selectedCities');
    $optionsStates = get_option('selectedStates');

    $queryStates = "(".implode(',', $optionsStates).")";
    $res         = $wpdb->get_results("select cityId from $cityTableName where stateId in $queryStates");
    $master      = [];
    foreach ($res as $cty) {
        $master[] = $cty->cityId;
    }
    $optionsCities = array_unique(array_merge($master, $optionsCities));
    $queryCities   = "(".implode(',', $optionsCities).")";
    $columnNames   = "date_time, city_name, currency, dieselDiff, petrolDiff, petrol, diesel";
    $sql           = "SELECT distinct $columnNames  FROM $tableName WHERE date(date_time)=(SELECT max(date_time) FROM $tableName) and cityId in $queryCities";
    $query         = $wpdb->prepare($sql);
    $result        = $wpdb->get_results($query);

    return $result;
}

function getCityDataDB($cityId, $top = 10)
{
    global $wpdb;
    $tableName   = $wpdb->prefix.'fuel_prices';
    $columnNames = "date_time, city_name, currency, dieselDiff, petrolDiff, petrol, diesel";
    $sql         = "SELECT distinct $columnNames FROM $tableName WHERE cityId=%d order by date_time desc limit %d";
    $query       = $wpdb->prepare($sql, $cityId, $top);

    $result = $wpdb->get_results($query);

    return $result;
}

function getCityDataDBByName($cityName, $top = 10)
{
    global $wpdb;
    $tableName   = $wpdb->prefix.'fuel_prices';
    $columnNames = "date_time, city_name, currency, dieselDiff, petrolDiff, petrol, diesel";
    $sql         = "SELECT distinct  $columnNames FROM $tableName WHERE LOWER(REPLACE(`city_name`, ' ', ''))=%s order by date_time desc limit %d";
    $query       = $wpdb->prepare($sql, strtolower(trim($cityName)), $top);
    $result      = $wpdb->get_results($query);

    return $result;
}

function getLastDate()
{
    global $wpdb;
    $tableName = $wpdb->prefix.'fuel_prices';

    $result = $wpdb->get_results("select max(date_time) as date from $tableName");

    return $result[0]->date;
}

function getFuelPriceByDate($cityName, $date)
{
    global $wpdb;
    $tableName   = $wpdb->prefix.'fuel_prices';
    $columnNames = "date_time, city_name, currency, dieselDiff, petrolDiff, petrol, diesel";
    $sql         = "SELECT distinct  $columnNames FROM $tableName WHERE LOWER(REPLACE(`city_name`, ' ', ''))=%s and date(date_time)=%s";
    $query       = $wpdb->prepare($sql, strtolower(trim($cityName)), $date);

    return $wpdb->get_results($query);
}
function getGoldPriceByDate($cityName, $date)
{
    global $wpdb;
    $tableName   = $wpdb->prefix.'fuel_gold';
    $columnNames = "date_time, city, price1, price2, change1, change2";
    $sql         = "SELECT distinct  $columnNames FROM $tableName WHERE LOWER(REPLACE(`city`, ' ', ''))=%s and date(date_time)=%s";
    $query       = $wpdb->prepare($sql, strtolower(trim($cityName)), $date);

    return $wpdb->get_results($query);
}

function getCityTrend($cityName, $fuelType, $months = 12)
{
    $today  = date_create(date("Y-m-01"));
    $master = [];
    for ($i = 0; $i < $months; $i++) {
        $year      = date("Y", strtotime("-".$i." months"));
        $monthName = date("F", strtotime("-".$i." months"));
        $month     = date("m", strtotime("-".$i." months"));
        $trend     = getTrendOnMonth($cityName, $fuelType, $year, $month);
        if ($trend) {
            $master[$monthName." ".$year] = $trend;
        }
    }

    return $master;
}

function getTrendOnMonth($cityName, $fuelType, $year, $month)
{
    $lastDate  = getLastDateOfMonth($cityName, $year, $month, "max");
    $firstDate = getLastDateOfMonth($cityName, $year, $month, "min");

    $firstDayRecord = getPriceOnDay($cityName, $firstDate);
    $lastDayRecord  = getPriceOnDay($cityName, $lastDate);

    if (!$firstDayRecord && !$lastDayRecord) {
        return [];
    }

    $firstPrice = (float)$firstDayRecord->$fuelType;
    $lastPrice  = (float)$lastDayRecord->$fuelType;

    $changePercentage = (float)(($lastPrice - $firstPrice) * 100 / $lastPrice);


    return [
        "first"   => $firstDayRecord,
        "last"    => $lastDayRecord,
        "highest" => getMaxOrMinFuelPrice($cityName, $fuelType, $year, $month, "max"),
        "lowest"  => getMaxOrMinFuelPrice($cityName, $fuelType, $year, $month, "min"),
        "trend"   => ($lastDayRecord > $firstDayRecord) ? "Increasing" : "Decreasing",
        "change"  => round($changePercentage, 2),
    ];
}

function getMaxOrMinFuelPrice($cityName, $fuelType, $year, $month, $clu)
{
    global $wpdb;
    $tableName = $wpdb->prefix.'fuel_prices';
    $sql       = "SELECT $fuelType as price, DATE_FORMAT(date_time, '%M') as month, GROUP_CONCAT(DATE_FORMAT(date_time, '%D')) as date from wp_fuel_prices WHERE petrol=(select $clu($fuelType) from $tableName WHERE month(date_time)='%s' and year(date_time)='%s' and city_name='%s') and  month(date_time)='%s' and year(date_time)='%s' and city_name='%s' group by $fuelType";

    $query = $wpdb->prepare($sql, $month, $year, $cityName, $month, $year, $cityName);

    return $wpdb->get_results($query)[0];
}

function getLastDateOfMonth($cityName, $year, $month, $clu)
{
    global $wpdb;
    $tableName = $wpdb->prefix.'fuel_prices';
    $sql       = "select $clu(date_time) as date from $tableName where city_name=%s and month(date_time)=$month and year(date_time)=$year";

    $query = $wpdb->prepare($sql, $cityName);

    return date('Y-m-d', strtotime($wpdb->get_results($query)[0]->date));

}

function getPriceOnDay($cityName, $date)
{
    global $wpdb;
    $tableName   = $wpdb->prefix.'fuel_prices';
    $columnNames = "DATE_FORMAT(date_time, '%D, %M') as day, date_time, city_name, currency, dieselDiff, petrolDiff, petrol, diesel";
    $sql         = "select $columnNames from $tableName WHERE city_name=%s and date(date_time)=%s";
    $query       = $wpdb->prepare($sql, $cityName, $date);
    $result      = $wpdb->get_results($query);

    if (count($result)) {
        return $result[0];
    }

    return [];
}
