<?php
require_once(ABSPATH.'wp-admin/includes/upgrade.php');
function create_database()
{
    global $wpdb;
    $state_table_name = $wpdb->prefix.'fuel_states';
    $city_table_name  = $wpdb->prefix.'fuel_cities';
    $price_table_name = $wpdb->prefix.'fuel_prices';
    $gold_table_name  = $wpdb->prefix.'fuel_gold';

    $sql  = "CREATE TABLE IF NOT EXISTS $state_table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		state_name varchar(55) DEFAULT '' NOT NULL,
		PRIMARY KEY  (id)
	);";
    $sql2 = "CREATE TABLE IF NOT EXISTS $city_table_name (
		id integer(12) NOT NULL AUTO_INCREMENT,
		cityId integer(10) DEFAULT 0 NOT NULL,
		stateId integer(10) DEFAULT 0 NOT NULL,
		city_name varchar(55) DEFAULT '' NOT NULL,
		stateName varchar(55) DEFAULT '' NOT NULL,
		lng float(2) DEFAULT 0 NOT NULL,
		lat float(2) DEFAULT 0 NOT NULL,
		PRIMARY KEY  (id)
	);";
    $sql3 = "CREATE TABLE IF NOT EXISTS $price_table_name (
		id integer(12) NOT NULL AUTO_INCREMENT,
		date_time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		cityId integer(10) DEFAULT 0 NOT NULL,
		stateId integer(10) DEFAULT 0 NOT NULL,
		city_name varchar(55) DEFAULT '' NOT NULL,
		stateName varchar(55) DEFAULT '' NOT NULL,
		currency varchar(55) DEFAULT '' NOT NULL,
		createdAt varchar(55) DEFAULT '' NOT NULL,
		changeText varchar(55) DEFAULT '' NOT NULL,
		dieselDiff float(2) DEFAULT 0 NOT NULL,
		petrolDiff float(2) DEFAULT 0 NOT NULL,
		petrol float(2) DEFAULT 0 NOT NULL,
		diesel float(2) DEFAULT 0 NOT NULL,
		PRIMARY KEY  (id)
	);";
    $sql4 = "CREATE TABLE IF NOT EXISTS $gold_table_name (
		id integer(12) NOT NULL AUTO_INCREMENT,
		date_time date DEFAULT '0000-00-00' NOT NULL,
		city varchar(55) DEFAULT '' NOT NULL,
		price1 float(2) DEFAULT 0 NOT NULL,
		price2 float(2) DEFAULT 0 NOT NULL,
		change1 float(2) DEFAULT 0 NOT NULL,
		change2 float(2) DEFAULT 0 NOT NULL,
		PRIMARY KEY  (id)
	);";
    dbDelta($sql);
    dbDelta($sql2);
    dbDelta($sql3);
    dbDelta($sql4);
    insertStatesAndCities();
}

function insertStatesAndCities()
{
    global $wpdb;
    $state_table_name = $wpdb->prefix.'fuel_states';
    $city_table_name  = $wpdb->prefix.'fuel_cities';


    $url  = "https://daily-fuel-prices-update-india.p.rapidapi.com/car/v2/fuel/states";
    $data = getData($url);


    //Insert states
    $sql_insert_state_data = "INSERT INTO $state_table_name(id, state_name) VALUES";
    foreach ($data->data->states as $state) {
        $master1[] = "(".implode(',', [$state->id, "'".$state->name."'"]).")";
    }
    $sql1  = $sql_insert_state_data.implode(',', $master1);
    $count = $wpdb->query($sql1);
    loge("State Inserted", "States inserted");

    //Insert Cities
    $sql_insert_city_data = "INSERT INTO $city_table_name(id,cityId, stateId, city_name, stateName, lng, lat) VALUES";

    foreach ($data->data->cities as $state) {
        foreach ($state as $city) {
            $master[] = "(".implode(',', [
                    $city->stateId.$city->id,
                    $city->id,
                    $city->stateId,
                    "'".$city->name."'",
                    "'".$city->stateName."'",
                    $city->lng ?? "0",
                    $city->lat ?? "0",
                ]).")";
        }
    }
    $sql = $sql_insert_city_data.implode(',', $master);
    $wpdb->query($sql);
    loge("Cities Inserted", "Cities inserted");

}

