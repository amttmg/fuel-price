<?php include('partials/header.php'); ?>
<table class="dt4">
    <thead>
    <tr>
        <th>Dates</th>
        <th>Petrol Price</th>
        <th>Diesel Price</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($data)) { ?>
        <?php foreach ($data as $dt) {
            $is_decrease_petrol = ($dt->petrolDiff < 0);
            $is_increase_petrol = ($dt->petrolDiff > 0);
            $is_decrease_diesel = ($dt->dieselDiff < 0);
            $is_increase_diesel = ($dt->dieselDiff > 0);
            ?>
            <tr>
                <td>
                    <?php echo date('Y-m-d', strtotime($dt->date_time)) ?>
                </td>
                <td>
                    <?php echo get_option('currency').' '.($dt->petrol) ?>
                    <?php if ($is_increase_petrol) { ?>
                        <span style="color: green" class="dashicons dashicons-arrow-up"></span>
                    <?php } else {
                        if ($is_decrease_petrol) { ?>
                            <span style="color: red" class="dashicons dashicons-arrow-down"></span>
                        <?php } else { ?>
                            <span style="color: green" class="dashicons dashicons-leftright"></span>
                        <?php }
                    } ?>
                </td>
                <td>
                    <?php print_r(get_option('currency').' '.$dt->diesel) ?>
                    <?php if ($is_increase_petrol) { ?>
                        <span style="color: green" class="dashicons dashicons-arrow-up"></span>
                    <?php } else {
                        if ($is_decrease_diesel) { ?>
                            <span style="color: red" class="dashicons dashicons-arrow-down"></span>
                        <?php } else { ?>
                            <span style="color: green" class="dashicons dashicons-leftright"></span>
                        <?php }
                    } ?>
                </td>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
<?php include('partials/footer.php') ?>
<script>
  $(document).ready(function() {
    if (!$.fn.DataTable.isDataTable('.dt4')) {
      $('.dt4').DataTable(
        {
          "pageLength": 50,
          "order": [[0, "desc"]],
          responsive: {
            breakpoints: [
              {name: 'desktop', width: Infinity},
              {name: 'tablet', width: 1024},
              {name: 'fablet', width: 768},
              {name: 'phone', width: 480}
            ]
          },
        }
      );
    }
  });
</script>
