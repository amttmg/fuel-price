<?php include('partials/header.php'); ?>
<table class="dt3">
    <thead>
    <tr>
        <th>Dates</th>
        <th>22k</th>
        <th>24k</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($data)) { ?>
        <?php foreach ($data as $dt) {
            $is_decrease_gold_22 = ($dt->change1 < 0);
            $is_increase_gold_22 = ($dt->change1 > 0);
            $is_decrease_gold_24 = ($dt->change2 < 0);
            $is_increase_gold_24 = ($dt->change2 > 0);
            ?>
            <tr>
                <td>
                    <?php echo date(get_option('date_format'), strtotime($dt->date_time)) ?>
                </td>
                <td>
                    <?php echo get_option('currency').' '.($dt->price1) ?>
                    <?php if ($is_increase_gold_22) { ?>
                        <span style="color: green" class="dashicons dashicons-arrow-up"></span>
                    <?php } else {
                        if ($is_decrease_gold_22) { ?>
                            <span style="color: red" class="dashicons dashicons-arrow-down"></span>
                        <?php } else { ?>
                            <span style="color: green" class="dashicons dashicons-leftright"></span>
                        <?php }
                    } ?>
                </td>
                <td>
                    <?php print_r(get_option('currency').' '.$dt->price2) ?>
                    <?php if ($is_increase_gold_22) { ?>
                        <span style="color: green" class="dashicons dashicons-arrow-up"></span>
                    <?php } else {
                        if ($is_decrease_gold_24) { ?>
                            <span style="color: red" class="dashicons dashicons-arrow-down"></span>
                        <?php } else { ?>
                            <span style="color: green" class="dashicons dashicons-leftright"></span>
                        <?php }
                    } ?>
                </td>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
<?php include('partials/footer.php'); ?>
