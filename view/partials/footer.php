<script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="//cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
    .dataTables_wrapper .dataTables_length select {
        margin-bottom: 20px;
    }

    @media screen and (max-width: 640px) {
        .dataTables_wrapper .dataTables_filter {
            margin-top: 0.5em;
            margin-bottom: 20px;
        }
    }

    .dataTables_info {
        margin-bottom: 30px;
    }
</style>