<?php include('partials/header.php'); ?>
<?php $randId = 'chart-'.rand(0, 100) ?>
<canvas id="<?php echo $randId ?>" style="height: 350px"></canvas>
<?php include('partials/footer.php'); ?>
<?php
$labels  = [];
$p_today = [];
$d_today = [];
foreach ($data as $dt) {
    $labels[]  = date(get_option('date_format'), strtotime($dt->date_time));
    $p_today[] = $dt->price1;
    $d_today[] = $dt->price2;
}
?>
<script>
  var ctx = document.getElementById('<?php echo $randId ?>').getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: <?php echo json_encode($labels) ?>,
      datasets: [
        {
          label: '22k',
          data: <?php echo json_encode($p_today) ?>,
          fill: false,
          borderColor: [
            'rgba(54, 162, 235, 1)'
          ],
          borderWidth: 2
        },
        {
          label: '24k',
          data: <?php echo json_encode($d_today) ?>,
          fill: false,
          borderColor: [
            'rgba(255, 99, 132, 1)'
          ],
          borderWidth: 2
        }
      ]
    },
    options: {
      scales: {
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: '<?php echo $gram ?> Gram Gold Price in INR'
          },
          ticks: {
            beginAtZero: false
          }
        }],
        xAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Date'
          }
        }],
      }
    }
  });
</script>

