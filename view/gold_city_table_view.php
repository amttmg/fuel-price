<?php include('partials/header.php'); ?>
<table id="dt2">
    <thead>
    <tr>
        <th>Cities</th>
        <th>22k</th>
        <th>Change</th>
        <th>24k</th>
        <th>Change</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($data)) { ?>
        <?php foreach ($data as $dt) {
            $is_decrease_gold_22 = ($dt->change1 < 0);
            $is_increase_gold_22 = ($dt->change1 > 0);
            $is_decrease_gold_24 = ($dt->change2 < 0);
            $is_increase_gold_24 = ($dt->change2 > 0);
            ?>
            <tr>
                <td>
                    <a href="?city=<?php echo strtolower(str_replace(' ', '',
                        $dt->city)) ?>"><?php echo $dt->city ?></a>
                </td>
                <td>
                    <?php echo get_option('currency').' '.($dt->price1) ?>
                </td>
                <td>
                    <?php echo get_option('currency').' '.($dt->change1) ?>
                    <?php if ($is_increase_gold_22) { ?>
                        <span style="color: green" class="dashicons dashicons-arrow-up"></span>
                    <?php } else {
                        if ($is_decrease_gold_22) { ?>
                            <span style="color: red" class="dashicons dashicons-arrow-down"></span>
                        <?php } else { ?>
                            <span style="color: green" class="dashicons dashicons-leftright"></span>
                        <?php }
                    } ?>
                </td>
                <td>
                    <?php print_r(get_option('currency').' '.$dt->price2) ?>
                </td>
                <td>
                    <?php echo get_option('currency').' '.($dt->change2) ?>
                    <?php if ($is_increase_gold_22) { ?>
                        <span style="color: green" class="dashicons dashicons-arrow-up"></span>
                    <?php } else {
                        if ($is_decrease_gold_24) { ?>
                            <span style="color: red" class="dashicons dashicons-arrow-down"></span>
                        <?php } else { ?>
                            <span style="color: green" class="dashicons dashicons-leftright"></span>
                        <?php }
                    } ?>
                </td>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
<?php include('partials/footer.php'); ?>
<script>
  $(document).ready(function() {
    $('#dt2').DataTable(
      {
        "pageLength": 50,
        responsive: {
          breakpoints: [
            {name: 'desktop', width: Infinity},
            {name: 'tablet', width: 1024},
            {name: 'fablet', width: 768},
            {name: 'phone', width: 480}
          ]
        },
      }
    );
  });
</script>
