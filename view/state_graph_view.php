<?php include('partials/header.php'); ?>
<canvas id="myChart" style="height: 350px"></canvas>
<?php include('partials/footer.php'); ?>
<?php
$labels  = [];
$p_today = [];
$d_today = [];
foreach ($data as $dt) {
    $labels[]  = $dt->city_name;
    $p_today[] = $dt->petrol;
    $d_today[] = $dt->diesel;
}
?>
<script>
  var ctx = document.getElementById('myChart').getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: <?php echo json_encode($labels) ?>,
      datasets: [
        {
          label: 'Petrol',
          data: <?php echo json_encode($p_today) ?>,
          fill: false,
          borderColor: [
            'rgba(54, 162, 235, 1)'
          ],
          borderWidth: 2
        },
        {
          label: 'Diesel',
          data: <?php echo json_encode($d_today) ?>,
          fill: false,
          borderColor: [
            'rgba(255, 99, 132, 1)'
          ],
          borderWidth: 2
        }
      ]
    },
    options: {
      scales: {
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Fuel Price in INR'
          },
          ticks: {
            beginAtZero: false
          }
        }],
      }
    }
  });
</script>

