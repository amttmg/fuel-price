<?php include('partials/header.php'); ?>
<div id="accordion">
    <?php foreach ($data as $key => $row) { ?>
        <h3><?php echo $key ?></h3>
        <div>
            <table class="trend-table">
                <tr>
                    <th colspan="2"><?php echo ucfirst($label ?? $fuelType) ?> Price</th>
                </tr>
                <tr>
                    <td width="50%"><?php echo $row["first"]->day ?></td>
                    <td width="50%">Rs.<?php echo $row["first"]->$fuelType*$gram ?></td>
                </tr>
                <tr>
                    <td width="50%"><?php echo $row["last"]->day ?></td>
                    <td width="50%">Rs.<?php echo $row["last"]->$fuelType*$gram ?></td>
                </tr>
                <tr>
                    <td width="50%">Highest on <?php echo $row["highest"]->month ?></td>
                    <td width="50%">Rs.<?php echo $row["highest"]->price*$gram ?>
                        <?php echo "on ".$row["highest"]->date.' '.$row["highest"]->month ?>
                    </td>
                </tr>
                <tr>
                    <td width="50%">Lowest on <?php echo $row["lowest"]->month ?></td>
                    <td width="50%">Rs.<?php echo $row["lowest"]->price*$gram ?>
                        <?php echo "on ".$row["lowest"]->date." ".$row["lowest"]->month ?>
                    </td>
                </tr>
                <tr>
                    <td>Overall Performance</td>
                    <td><?php echo $row["trend"] ?></td>
                </tr>
                <tr>
                    <td>Change %</td>
                    <td><?php echo (($row["change"] > 0) ? '+' : '').$row["change"] ?>%</td>
                </tr>
            </table>
        </div>
    <?php } ?>
</div>
<?php include('partials/footer.php'); ?>
<style>
    .trend-table td {
        padding: 10px;
    }

    .trend-table {
        background-color: #fdfdfd;
    }

    .trend-table th {
        padding: 10px;
        text-align: center;
        background-color: #f1f1f1;
    }

    .ui-accordion-header.ui-state-active {
        border: 1px solid #c5c5c5;
        background: #f6f6f6;
        font-weight: normal;
        color: #454545;
    }

    .ui-state-active .ui-icon, .ui-button:active .ui-icon {
        background-image: url(images/ui-icons_ffffff_256x240.png);
        color: black;
    }

    .ui-accordion .ui-accordion-content {
        padding: 10px;
        border-top: 0;
        overflow: auto;
    }
</style>
<script>
  $(function() {
    $("#accordion").accordion({
      heightStyle: "content",
      collapsible: true,
    });
  });
</script>
