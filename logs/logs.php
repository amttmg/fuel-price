<?php
function loge($title, $message, $type = "info")
{
    $date = new DateTime("now");
    $myfile = fopen("logs.txt", "a") or die("Unable to open file!");
    $txt = $date->format("Y-m-d h:i:s")."    ".$type."   ".$title."      ".$message."\n";
    fwrite($myfile, $txt);
    fclose($myfile);
}
