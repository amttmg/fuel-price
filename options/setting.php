<?php
function myplugin_register_settings()
{
    add_option('currency', 'INR');
    add_option('api_key', '');
    add_option('api_host', '');
    add_option('selectedCities', '');
    add_option('selectedStates', '');
    register_setting('myplugin_options_group', 'currency', 'myplugin_callback');
    register_setting('myplugin_options_group', 'api_key', 'myplugin_callback');
    register_setting('myplugin_options_group', 'api_host', 'myplugin_callback');
    register_setting('myplugin_options_group', 'selectedCities', 'myplugin_callback');
    register_setting('myplugin_options_group', 'selectedStates', 'myplugin_callback');
}

function myplugin_register_options_page()
{
    add_options_page('Fuel Price In India', 'Fuel Price Setting', 'manage_options', 'fuel-price',
        'myplugin_options_page');
}

add_action('admin_menu', 'myplugin_register_options_page');
add_action('admin_init', 'myplugin_register_settings');

function myplugin_options_page()
{
    $states       = getStateWithCities();
    $options      = get_option('selectedCities');
    $optionsState = get_option('selectedStates');
    include('page.php');
    //fetchDataFromApiAndInsert();
}
