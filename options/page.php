<div class="notice notice-success settings-error" style="margin-top: 50px">
    <h2>Fuel Price Setting</h2>
    <form method="post" action="options.php">
        <?php settings_fields('myplugin_options_group'); ?>
        <table>
            <tr valign="top">
                <th scope="row"><label for="currency">Currency: </label></th>
                <td><input type="text" id="currency" name="currency"
                           value="<?php echo get_option('currency'); ?>"/></td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="api_host">x-rapidapi-host: </label></th>
                <td><input style="width: 500px" type="text" id="api_host" name="api_host"
                           value="<?php echo get_option('api_host'); ?>"/></td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="api_key">x-rapidapi-key: </label></th>
                <td><input style="width: 500px" type="text" id="api_key" name="api_key"
                           value="<?php echo get_option('api_key'); ?>"/></td>
            </tr>
            <tr>
                <th scope="row"><label for="api_key">Active Shortcodes: </label></th>
                <td><?php include "shortcodes.php"; ?></td>
            </tr>
        </table>
        <?php submit_button(); ?>

        <br/>
        <br/>
        <table border="1" width="100%">
            <thead>
            <tr>
                <th>StateId/cityId</th>
                <th>State/City</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($states as $state) { ?>
                <tr>
                    <th><?php echo $state->id ?></th>
                    <th><?php echo $state->state_name ?></th>
                    <th>
                        <input type="checkbox"
                               name="selectedStates[<?php echo $state->id ?>]"
                               value="<?php echo $state->id ?>"
                            <?php checked($state->id == $optionsState[$state->id]); ?>
                        />
                    </th>
                </tr>
                <?php foreach ($state->cities as $city) { ?>
                    <tr>
                        <td><?php echo $city->cityId?></td>
                        <td><?php echo $city->city_name ?></td>
                        <td style="text-align: center;">
                            <input type="checkbox"
                                   name="selectedCities[<?php echo $city->id ?>]"
                                   value="<?php echo $city->cityId ?>"
                                <?php checked($city->cityId == $options[$city->id]); ?>
                            />
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
            </tbody>
        </table>
    </form>
</div>
