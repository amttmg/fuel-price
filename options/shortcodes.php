<?php
if ($options) {
    foreach ($options as $id => $city) {
        $cityDetails = getCityDetails($id);
        if ($cityDetails) {
            echo '[fuel-price cityId='.$cityDetails->cityId.' cityName='.$cityDetails->city_name.'] <br/>';
        }
    }
}
if ($optionsState) {
    foreach ($optionsState as $id => $state) {
        $stateDetails = getStateDetails($id);
        if ($stateDetails) {
            echo '[fuel-price stateId='.$stateDetails->id.' stateName='.$stateDetails->state_name.'] <br/>';
        }
    }
}

